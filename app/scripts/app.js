import svg4everybody from 'svg4everybody';
import $ from 'jquery';
// import './lib/jquery.magnific-popup.js';
// import './lib/slick.js';
import './lib/owl.carousel.js';
// import './lib/jquery-ui.js';
// import './lib/jquery.nicescroll.js';

$(() => {
	svg4everybody();
});

$(document).ready(function() {
	$('.header-carousel').owlCarousel({
    loop:true,
		items: 1,
		dots:true,
    nav:false
	});

	$('.brands-carousel').owlCarousel({
    loop:true,
		items: 5,
		center:true,
		dots:true,
    nav:true,
		navText: [,],
		responsive: {
			0: {
				items: 1
			},
			520: {
				items: 2
			},
			850: {
				items: 3
			},
			1300: {
				items: 4
			},
			1500: {
				items: 5
			}
		}
	});

	$('.partners-text-carousel').owlCarousel({
    loop:false,
		items: 1,
		dots:true,
    nav:false
	});

	$('.hamburger').click(function () {
		$(this).toggleClass('is-active');
		$('.js-nav').slideToggle('fast');
	})
});
